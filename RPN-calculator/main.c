#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

#include"stack.h"


/* Function prototypes */
double reverse_polish_calc(char *); 
char *parse_file(char *filename);


int main(int argc, char *argv[])
{ 
    double result;
    if (argc == 2) {
        char *input = parse_file(argv[1]);
        result = reverse_polish_calc(input);
    } else {
        fprintf(stderr, "Error: None of the supported input methods were used\n");
        return EXIT_FAILURE;
    }
    
    printf("The final result is: %.2f\n", result);
    return EXIT_SUCCESS;
}


char *
parse_file(char *filename)
{
    FILE *fp;
    char input[BUFSIZ];
    if (!(fp = fopen(filename, "r"))) {
        fprintf(stderr, "The file %s does not exist\n", filename);
        exit(EXIT_FAILURE);
    }
        
    if (!fgets(input, sizeof(input) - 1, fp))
        fprintf(stderr, "Error in %s", filename);
 
    fclose(fp);
    input[strlen(input) - 1] = '\0';
    
    char *tmp;
    if (!(tmp = malloc(BUFSIZ)))
        fprintf(stderr, "Error allocating memory");
    strcpy(tmp, input);
    
    return tmp;
}


double
reverse_polish_calc(char *input)
{
    int num;
    char curr;
    for (int index = 0; index < strlen(input); index++) { 
        curr = *(input + index);
        if (isspace(curr)) {
            continue;
        }

        if (isdigit(curr)) {
            num = curr - '0';
            for (++index; isdigit(*(input + index)); ++index) {
                num = (num * 10) + (*(input + index) - '0');
            }

            push(num);
        } else {
            switch (curr) {
                double tmp;
                case '+':
                    push(pop() + pop());
                    break;
                case '-':
                    tmp = pop();
                    push(pop() - tmp);
                    break;
                case '/':
                    tmp = pop();
                    push(pop() / tmp);
                    break;
                case '*':
                    push(pop() * pop());
                    break;
                default:
                    fprintf(stderr, "Error: Operation not found\n");
                    return EXIT_FAILURE;
            }
        }
    }
    return pop(); 
}
