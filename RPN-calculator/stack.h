#if !defined (STACK)
    
#define STACK

/* the maximum length of the stack array */
#define STACKBUF 50

/* pushes a given double onto the stack.
 * returns the status of the action:
 *  -> if error returns EXIT_FAILURE
 *  -> else returns EXIT_SUCCESS
 */
int push(double value);


/* returns the top-most value
 * on the stack
 */
double pop();


/* returns the value of the 
 * top-most item in the stack
 * without push()
 */
double peak();

#endif
