#include<stdlib.h>

#include"stack.h"

static double buff[STACKBUF];
/* 
 * Tracks the top-most object in a stack
 * if top = 5, then the top-most element is 
 * stored at buff[5]
 */
static int top = -1;

int
push(double value)
{

    if (top == STACKBUF)
        return EXIT_FAILURE;
    buff[++top] = value;
    return EXIT_SUCCESS;
}


double
pop()
{
    if (top <= -1)
        exit(EXIT_FAILURE);
    return buff[top--];
}

double
peak()
{
    return buff[top];
}
