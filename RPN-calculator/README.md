# Simple reverse polish notation calculator

My first real program in C. 
It only computes integer values.



## How to breakdown RPN
The reverse polish notation doesn't use brackets
like normal mathematical notation does, instead
the operator is placed after the operands. As an
example, "2 * (5 + 4)" adds 5 and 4, then, multiplies
this result by 2, thus the RPN equivalent becomes
"5 4 + 2 *".

### Examples
* "2 + 4" in RPN is "2 4 +"
* "2 * (5 + 4)" in RPN is "5 4 + 2 *"


## Usage
* ./rpc \<filename\>

The format of the file should include spaces between
the numbers and the operations.
"2 4 +" is valid.
"24 +" is invalid (this is seen as only one operand)
