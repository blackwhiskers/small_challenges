#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>


char *ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char *alpha = "abcdefghijklmnopqrstuvwxyz";

char
shift_char(char input, int shift)
{
    int alpha_len = strlen(ALPHA);
    int new_i;
    for (int i = 0; i < alpha_len; ++i) {
        if (tolower(input) == *(alpha + i)) {
            new_i = ((i + shift) % alpha_len);
            break;
        }
    }
    if (islower(input))
        return *(alpha + new_i);
    else if (isupper(input))
        return *(ALPHA + new_i);
    else
        return -1;
}


int
main(int argc, char *argv[])
{
    if (argc != 3) {
        fprintf(stderr, "Wrong usage: ./caesar <string> <shift>\n");
        return -1;
    }
    char buf[strlen(*(argv + 1)) + 1];
    int shift = atoi(*(argv + 2));
    char curr;
    for (int i = 0; i < strlen(*(argv + 1)); ++i) {
        curr = *(*(argv + 1) + i);
        if ((*(buf + i) = shift_char(curr, shift)) == -1) {
            fprintf(stderr, "%c is illegal.\n", curr);
            return -1;
        }
    }
    buf[strlen(*(argv + 1))] = '\0';
    printf("%s\n", buf);
    return 0;
}
